class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def take_turn
    @guesser.guess
    checked = @referee.check_guess(@guesser.guess)
    update_board(@guess, checked)
    @guesser.handle_response(@guess, checked)
  end

  def update_board(guess, positions)
    positions.each do |el|
      board[el] = guess
    end
  end

end

class HumanPlayer


end

class ComputerPlayer
  attr_accessor :dictionary, :secret_word, :candidate_words

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = dictionary.sample
    @secret_word.length
  end

  def check_guess(letter)
    correct_idxs = []
    secret_arr = @secret_word.split("")
    secret_arr.each_with_index do |el, idx|
      if letter == el
        correct_idxs << idx
      end
    end
    correct_idxs
  end

  def register_secret_length(length)
    @candidate_words = @dictionary.select { |el| el if el.size == length }
  end

  def guess(board)
    cand_letters = @candidate_words.join.chars - board.join.chars
    hsh = Hash.new(0)
    cand_letters.each do |el|
      hsh[el] += 1
    end
    most_common_letter_value = hsh.values.max
    mod_hash = hsh.reject { |k, v| v == most_common_letter_value }
    selected_key = hsh.keys - mod_hash.keys
    selected_key[0]
  end

  def handle_response(letter, pos)
    length = @candidate_words[0].length
    neg_pos = (0..length).to_a - pos
    pos.each do |idx|
      @candidate_words.select! { |word| word[idx] == letter }
    end
    neg_pos.each do |idx|
      @candidate_words.select! { |word| word[idx] != letter}
    end
    @candidate_words
  end



end
